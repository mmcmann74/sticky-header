function r(number, precision) {
  if (!precision) precision = 0;
  var factor = Math.pow(10, precision);
  return Math.round(number * factor) / factor;
}
var stickyHeaders = (function () {

  var $window = $(window),
    $stickies
    cssRuleAdded = false;

  var load = function (stickies, reset) {
    if (!reset) reset = false;
    if (typeof stickies === "object" && stickies instanceof jQuery && stickies.length > 0) {
      $stickies = stickies.each(function () {
        var $thisSticky, $isTopSticky;
        if (reset) {
          $thisSticky = $(this);
          $thisSticky.removeClass("absolute fixed").removeAttr("style");
        } else {
          $thisSticky = $(this).wrap('<div class="followWrap" />');
        }
        $isTopSticky = $(this).hasClass("first");
        $thisSticky
          .data('firstOne', $isTopSticky)
          .data('originalPosition', $thisSticky.offset().top)
          .data('originalHeight', $thisSticky.outerHeight())
          .parent()
          .height($thisSticky.outerHeight());
        if ($isTopSticky) {
          $topOne = $thisSticky;
          if (!cssRuleAdded || reset) {
            $("#extra-style-block").remove();
            $("<style id='extra-style-block' type='text/css'>.followMeBar.fixed{top:" + $topOne.outerHeight() + "px;}</style>").appendTo("head");
            cssRuleAdded = true;
          }
        }
        if (reset) _whenScrolling();
      });
      $window.off("scroll.stickies").on("scroll.stickies", function () { _whenScrolling(); });
    }
  };

  var _whenScrolling = function () {
    var $topOne;
    $stickies.each(function (i) {
      var $thisSticky = $(this),
        $isTopSticky = $thisSticky.hasClass("first");
      if ($isTopSticky) {
        $topOne = $thisSticky;
        if (!cssRuleAdded) {
          $("<style type='text/css'>.followMeBar.fixed{top:" + $topOne.outerHeight() + "px;}</style>").appendTo("head");
          cssRuleAdded = true;
        }
      }
    });
    $stickies.each(function (i) {
      var $thisSticky = $(this),
        $isTopSticky = !!$thisSticky.data("firstOne"),
        $stickyYPos = $thisSticky.data('originalPosition'),
        topLineY = $isTopSticky ? $window.scrollTop() : $window.scrollTop() + $topOne.outerHeight();
      $("#top-line").css("top", topLineY);
      //console.clear();
      if (i === 0) $("#output-1").html(p());
      if (i === 1) $("#output-2").html(p());
      if ($stickyYPos <= topLineY) { // el is ABOVE topLine
        var $nextSticky = $stickies.eq(i + 1),
          $nextStickyPosition = r($nextSticky.data('originalPosition') -
            $thisSticky.data('originalHeight'));
        $thisSticky.addClass("fixed"); //.css('top', $isTopSticky ? 0 : $topOne.outerHeight());
        //console.log(`$stickyYPos <= topLineY (${$stickyYPos} <= ${topLineY})`);
        //$thisSticky.css('top', $topOne.outerHeight());
        // if (!$isTopSticky) $thisSticky.css("top", topLine);
        if ($nextSticky.length > 0 && $thisSticky.offset().top >= $nextStickyPosition && !$isTopSticky) {
          $thisSticky.addClass("absolute").css("top", $nextStickyPosition);
        }
      } else {
        var $prevSticky = $stickies.eq(i - 1);
        // $thisSticky.parent().css('top', $isTopSticky ? 0 : $topOne.outerHeight());
        $thisSticky.removeClass("fixed");
        if ($prevSticky.length > 0 && topLineY <= $thisSticky.data('originalPosition') - $thisSticky.data('originalHeight')) {
          $prevSticky.removeClass("absolute");
          //if (!$prevSticky.hasClass("fixed")) {
          //$prevSticky.css('top', $isTopSticky ? 0 : $topOne.outerHeight());
          $prevSticky.removeAttr("style");
          //}
        }
      }
      function p() {
        return "topLine/$window.scrollTop() = " + r(topLineY, 2) + "<hr>"
          + "class = <i>" + $thisSticky.attr("class") + "</i><hr>"
          + "style = <i>" + $thisSticky.attr("style") + "</i><hr>"
          + "$stickyPosition = " + r($stickyYPos, 4) + "<hr>"
          + "$thisSticky.data('originalPosition') = " + r($thisSticky.data('originalPosition'), 2) + "<hr>"
          + "$thisSticky.data('originalHeight') = " + r($thisSticky.data('originalHeight'), 2) + "<hr>"
          + "$thisSticky.offset().top = " + r($thisSticky.offset().top, 2) + "<hr>"
          ;
      }
    });
  };

  return {
    load: load
  };
})();

